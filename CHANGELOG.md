# Masque: Goldpaw Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).

## [1.0.3] 2017-08-29
### Changed
- Bumped the toc version to patch 7.3.0.

## [1.0.2] 2017-04-03
### Fixed
- Fixed an issue where a number was used instead of a string, causing the skin to bug out and not load.

## [1.0.1] 2017-03-29
### Changed
- Bumped toc to patch 7.2.0.

